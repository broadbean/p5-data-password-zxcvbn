#!/usr/bin/env perl
use strict;
use warnings;
use Data::Password::zxcvbn;
use Getopt::Long qw(:config
                    posix_default no_require_order
                    auto_version auto_help
               );
# VERSION
# PODNAME: zxcvbn-password-strength
# ABSTRACT: evaluate password strength

=head1 SYNOPSIS

  zxcvbn-password-strength [options] [password...]

Options:

=for :list
= C<--json>
= C<-j>
output a stream of JSON objects with full details for each password;
without this option, only the password and its score will be printed
= C<< --from <file> >>
= C<< -f <file> >>
opens the given C<file> and treats each line as a password to
evaluate; can be given more than once
= C<--help>
prints this help message and exits
= C<--version>
prints the script version and exits

=cut

my ($json,@from);
GetOptions(
    'json|j!' => \$json,
    'from|f=s' => \@from,
);

if ($json) {
    require JSON::MaybeXS;
    $json = JSON::MaybeXS->new(
        ascii => 1,
        pretty => 1,
        canonical => 1,
        allow_blessed => 1,
        convert_blessed => 1,
    );
}

sub check_one {
    my ($password) = @_;
    my $strength = Data::Password::zxcvbn::password_strength($password);
    if ($json) {
        print $json->encode({
            password => $password,
            strength => $strength,
        }),",\n";
    }
    else {
        print $password, ' -> ',$strength->{score},"\n";
    };
}

check_one($_) for @ARGV;
for my $file (@from) {
    open my $fh,'<:utf8',$file or die "Can't open $file: $!";
    while (my $password = <$fh>) {
        chomp $password;
        next unless $password;
        check_one($password);
    }
}
