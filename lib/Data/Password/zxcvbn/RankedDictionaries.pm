package Data::Password::zxcvbn::RankedDictionaries;
use strict;
use warnings;
use Data::Password::zxcvbn::RankedDictionaries::Common;
use Data::Password::zxcvbn::RankedDictionaries::English;
# VERSION
# ABSTRACT: ranked dictionaries for common English words

=head1 DESCRIPTION


=cut

our %ranked_dictionaries = (
    %Data::Password::zxcvbn::RankedDictionaries::Common::ranked_dictionaries,
    %Data::Password::zxcvbn::RankedDictionaries::English::ranked_dictionaries,
);

1;
