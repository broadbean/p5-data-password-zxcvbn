package Data::Password::zxcvbn::AdjacencyGraph;
use strict;
use warnings;
use Data::Password::zxcvbn::AdjacencyGraph::Common;
use Data::Password::zxcvbn::AdjacencyGraph::English;
# VERSION
# ABSTRACT: adjacency graphs for common English keyboards

=head1 DESCRIPTION

=cut

our %graphs = (
    %Data::Password::zxcvbn::AdjacencyGraph::Common::graphs,
    %Data::Password::zxcvbn::AdjacencyGraph::English::graphs,
);

1;
